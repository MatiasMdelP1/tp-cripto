SHELL := /bin/bash

up:
	cd server && pm2 start ecosystem.config.js

down:
	pm2 delete $(shell pm2 jlist | jq 'map(.name) | join(" ")' | sed 's/"//g')
.PHONY: down

logs:
	pm2 logs

install:
	cd client && npm i
	cd server && npm i

.PHONY: client
client:
	cd client && pm2 start ecosystem.config.js

.PHONY: malicious-client
malicious-client:
	cd client && pm2 start ecosystem.config.js && pm2 start run_malicious_client.js