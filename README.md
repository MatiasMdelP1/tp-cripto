# TP Cripto


El Tp cuenta con un mecanismo de Encriptado para la comunicacion entre pares y con mensajes firmados utilizando una firma digital para autenticar a los clientes.

Primero los pares intercambian las claves publicas, para ser usadas en el cifrado de sus respectivos mensajes.

Despues el cliente se registra en el servidor compartiendo una clave publica, para que el servidor autentique sus mensajes. 

Con esto se garantiza una firma digital y el encriptado de la comuncacion en transito.

## Informe

Compilacion del informe medinte Pandoc:

```
pandoc informe.md --pdf-engine=xelatex -o informe.pdf
```