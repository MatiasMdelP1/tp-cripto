module.exports = {
  apps : [{
    name: 'client',
    script: './',
    watch: '.',
    autorestart: false
  }],
};
