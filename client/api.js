const axios = require('axios')
const serverAddress = process.env.SERVER_ADDRESS || 'http://127.0.0.1:3000/'
const server = axios.create({ baseURL: serverAddress })


async function postMessage(path, data) {
    let res
    try {
        res = await server.post(path, data)
    } catch (err) {
        if (err.isAxiosError && err.response) {
            console.error(`${err.response.status}  ${err.response.statusText}`)
            console.error(err.response.data)
        } else if (err.isAxiosError) {
            console.error(err.message)
        } else {
            console.error(err)
        }
        return err
    }
    return res
}

async function register(clientId, publicKey, clientName) {
    let res = await postMessage(`/register/`, { clientId, publicKey, clientName })
    return res.data
}

async function log(clientId, data) {
    console.log("Sending log with data",data)
    let res = await postMessage(`/log/`, { clientId, ...data })
    return res.data
}

async function getClient(clientId, data) {
    console.log("Sending getClient with data", data)
    let res = await postMessage(`/getClient/`, { clientId, ...data })
    return res.data
}

// exchangeKeys()

module.exports = {
    register,
    log,
    getClient
}