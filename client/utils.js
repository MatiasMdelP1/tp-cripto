const crypto = require('crypto')
const fs = require('fs')
const path = require('path')

async function generateKeyPair (clientId) {

    const privateKeysPath = process.env.PRIVATE_KEYS_PATH || '.keys/'
    
    if (!fs.existsSync(privateKeysPath)) {
      fs.mkdirSync(privateKeysPath)
    }

    let keyPair
    try {
        keyPair = await crypto.generateKeyPairSync('rsa', {
            modulusLength: 4096,
            publicKeyEncoding: { type: 'spki', format: 'pem' },
            privateKeyEncoding: { type: 'pkcs8', format: 'pem' },
        })
    } catch (err) {
        console.error(err)
        return
    }

    try {
        fs.writeFileSync(
            path.join(privateKeysPath, clientId + '.pem'),
            keyPair.privateKey,
        )
    } catch (err) {
        console.error(err)
        return
    }
    return keyPair
}

function sign(privateKey, msg) {
        // Sign
    const signer = crypto.createSign('RSA-SHA512');
    signer.update(msg);
    const signature = signer.sign(privateKey, 'hex');
    return signature
}

module.exports = {
    generateKeyPair,
    sign
}