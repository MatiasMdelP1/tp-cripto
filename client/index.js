
const utils = require('./utils')
const api = require('./api')

const { v4: uuidv4 } = require('uuid')
const dotenv = require('dotenv')

dotenv.config()

let process_name = "CLIENT"
// adding a tag to all console logs!
var originalConsoleLog = console.log;
console.log = function () {
  args = [];
  args.push('[' + process_name + '] ');
  // Note: arguments is part of the prototype
  for (var i = 0; i < arguments.length; i++) {
    args.push(arguments[i]);
  }
  originalConsoleLog.apply(console, args);
};


exports.newClient = async function () {
  console.log("New Client")
  const clientId = uuidv4()
  const clientName = "trustworthy_client"

  let keyPair = await utils.generateKeyPair(clientId)
  api.register(clientId, keyPair.publicKey, clientName)
  let message = "New Message"
  data = {
    message: "New Message",
    sign: utils.sign(keyPair.privateKey, clientId)
  }

  api.log(clientId, data)
}

exports.newClient()