const { rejects } = require('assert');
const crypto = require('crypto');
const { resolve } = require('path');

const { db } = require('./db')

async function validateSign(clientId, sign) {
    return new Promise((resolve, reject) => {
        db.get(`SELECT public_key FROM CLIENTS where id = '${clientId}' `, (err, row) => {
            if (err) {
                console.error(err.message);
                reject(err.message)
            }
            if (row) {
                console.log(row)
                let signature = sign
                let publicKey = row.public_key
                console.log("Sign", signature)

                const verifier = crypto.createVerify('RSA-SHA512');
                verifier.update(clientId);
                const publicKeyBuf = new Buffer.from(publicKey, 'utf-8');
                const signatureBuf = new Buffer.from(signature, 'hex');
                const result = verifier.verify(publicKeyBuf, signatureBuf);
                if (result) {
                    console.log("Client ID Verified")
                    // NOW IT MUST LOG HERE
                    resolve("OK")
                }
                else {
                    console.log("INVALID SIGN")
                    reject()
                }
            }
        })
    })
}

async function logMessage(clientId, message) {
    return new Promise((resolve, reject) => {
        db.run(`INSERT INTO logs VALUES ("${clientId}", "${message}")`, err => {
            if (err) {
                reject(err)
            }
            else resolve()
        })

    })
}

async function registerKey(clientId, publicKey, clientName) {
    return new Promise((resolve, reject) => {
        db.run(`INSERT INTO clients VALUES ("${clientId}", "${publicKey}", "${clientName}")`, err => {
            if (err) {
                reject(err)
            }
            else resolve()
        })
    })
}

async function getClient(clientName) {
    return new Promise((resolve, reject) => {
        db.all(
            `SELECT client_name, public_key FROM clients 
            WHERE client_name="${clientName}"`
        , (err, rows) => {
            if (err) return reject(err)
            return resolve(rows)
        })
    })
}

module.exports = {
    validateSign,
    registerKey,
    logMessage,
    getClient
}