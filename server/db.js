const sqlite3 = require('sqlite3')

const dbConnection = process.env.DB_CONNECTION || 'db.sqlite'

let db = new sqlite3.Database(dbConnection)

function connect(dbConnection) {
  return new Promise((resolve, reject) => {
    db.on('open', resolve)
    db.on('error', reject)
  })
}

async function createSchema() {
  await new Promise((resolve, reject) => {
    db.run(
      'CREATE TABLE IF NOT EXISTS clients ( \
        id TEXT PRIMARY KEY,                \
        public_key TEXT NOT NULL UNIQUE,    \
        client_name TEXT NOT NULL           \
        ) WITHOUT ROWID;',
      err => {
        if (err) {
          reject(err)
        }
        else {
          resolve()
        }
      })
  })

  await new Promise((resolve, reject) => {
    db.run(
      'CREATE TABLE IF NOT EXISTS logs ( \
        clientId TEXT NOT NULL,                \
        message TEXT NOT NULL     \
        )',
      err => {
        if (err) {
          reject(err)
        }
        else {
          resolve()
        }
      })
  })
}


module.exports = {
  db,
  connect,
  createSchema
}