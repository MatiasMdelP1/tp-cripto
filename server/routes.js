const express = require('express')
const crypto = require('crypto')

const services = require('./services')
const { db } = require('./db')

const router = express.Router()

/**
 * Receives a public key with a client id and saves it on the database
 */
router.post('/register/', async function (req, res) {
  let { clientId, publicKey, clientName } = req.body
  console.log(`REGISTERING for clientId ${clientId}`)
  await services.registerKey(clientId, publicKey, clientName)
  return "OK"
})

/**
 * Receives an encrypted message with a client id. Verifies that the message
 * was encrypted with the client id private key and logs it.
 */
router.post('/log/', async function (req, res) {
  let { clientId, sign, message } = req.body
  console.log(`LOGGING for clientId ${clientId} with sign ${sign}`)
  try {
    await services.validateSign(clientId, sign)
    await services.logMessage(clientId, message)
    console.log('Message logged successfully!')
  }
  catch (err) {
    res.sendStatus(400)
  }

})

router.post('/getClient', async function (req, res) {
  let { clientId, sign, desiredClientName } = req.body
  console.log(`GET CLIENT for clientId ${clientId} with sign ${sign}`)
  let result
  try {
    await services.validateSign(clientId, sign)
    result = await services.getClient(desiredClientName)
  }
  catch (err) {
    console.log(err)
    res.sendStatus(400)
  }

  res.status(200).send(result)
})

module.exports = router
