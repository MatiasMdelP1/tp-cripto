const express = require('express')
const dotenv = require('dotenv')
const bodyParser = require('body-parser')

dotenv.config()

const db = require('./db')
const router = require('./routes')


let process_name = "SERVER"
// adding a tag to all console logs!
var originalConsoleLog = console.log;
console.log = function() {
    args = [];
    args.push( '[' + process_name + '] ' );
    // Note: arguments is part of the prototype
    for( var i = 0; i < arguments.length; i++ ) {
        args.push( arguments[i] );
    }
    originalConsoleLog.apply( console, args );
};


async function start(hostname, port) {
  try {
    await db.connect()
  } catch (err) {
    console.error(err)
    return
  }

  console.log('Succesfully connected to DB')

  db.createSchema()

  const app = express()

  // app.use(bodyParser.text())
  app.use(express.json());

  app.use(router)

  app.listen(port, hostname, () => {
    console.log(`Server listening at http://${hostname}:${port}`)
  })
}

const hostname = process.env.HOSTNAME || '0.0.0.0'
const port = process.env.PORT || 3000

start(hostname, port)
