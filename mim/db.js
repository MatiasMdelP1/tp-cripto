const sqlite3 = require('sqlite3')

const dbConnection = process.env.DB_CONNECTION || 'db.sqlite'

exports.db = new sqlite3.Database(dbConnection)

exports.connect = function (dbConnection) {
  return new Promise((resolve, reject) => {
    exports.db.on('open', resolve)
    exports.db.on('error', reject)
  })
}

exports.createSchema = function () {
  exports.db.run(
    'CREATE TABLE IF NOT EXISTS clients ( \
      id TEXT PRIMARY KEY,                \
      public_key TEXT NOT NULL UNIQUE     \
    ) WITHOUT ROWID;',
  )
}
