const express = require('express')
const axios = require('axios')
const { db } = require('./db')

const router = express.Router()


router.post('/*',async function(req, res) {
  console.log(req.data)
  console.log(req)
  let res1 = await axios.post(`http://customurl${req.url}`, req.data)
  return res1
})
/**
 * Receives a public key with a client id and saves it on the database
 */
// router.post('/register/:clientId', function (req, res) {
//   db.run(`INSERT INTO clients VALUES ("${req.params.clientId}", "${req.body}")`)
//   res.sendStatus(200)
// })

// /**
//  * Receives an encrypted message with a client id. Verifies that the message
//  * was encrypted with the client id private key and logs it.
//  */
// router.post('/log', function (req, res) {
//   console.log(req.body)
//   res.sendStatus(200)
// })

module.exports = router
