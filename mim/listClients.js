const dotenv = require('dotenv')

dotenv.config()

const db = require('./db')

async function listClients() {
  try {
    await db.connect()
  } catch (err) {
    console.error(err)
    return
  }

  db.db.each('SELECT * FROM clients;', (err, row) => {
    console.log(row)
  })
}

listClients()
